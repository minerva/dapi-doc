---
layout: default
title: Data API Homepage
order: 1
---

# Data API docs
Data API (DAPI) was developed to serve as a data endpoint providing content of external databases where API is unavailable.  
Moreover, DAPI provides basic licensing support, as users have to accept licenses of the datasets they access.

DAPI is used by the MINERVA Platform, starting from version 15 (see more on the [homepage](https://minerva.pages.uni.lu)).

Currently, DAPI provides access to the bulk content offered by:
- DrugBank ([see source and license](https://www.drugbank.ca/releases/latest))
- Comparative Toxicogenomic Database ([see source and license](https://ctdbase.org/downloads/))

## Direct use of DAPI

DAPI is meant to be primarily an API for the MINERVA Platform instances.   
You can use it directly, but you'll have to register an account and accept licenses of the datasets you want to access,  
using appropriate API calls.


Detailed documentation of API endpoints can be found [here](http://dapi.lcsb.uni.lu/docs/).
